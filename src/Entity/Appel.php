<?php

namespace App\Entity;

use App\Repository\AppelRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AppelRepository::class)
 */
class Appel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var TypeAppel
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeAppel", inversedBy="listeAppels")
     */
    private $typeAppel;

    /**
     * @ORM\Column(type="date")
     */
    private $dateAppel;

    /**
     * @ORM\Column(type="time")
     */
    private $heureAppel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dureeOrVolume;

    /**
     * @var Facture
     * @ORM\ManyToOne(targetEntity="App\Entity\Facture", inversedBy="listeAppels")
     */
    private $facture;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateAppel(): ?\DateTimeInterface
    {
        return $this->dateAppel;
    }

    public function setDateAppel(\DateTimeInterface $dateAppel): self
    {
        $this->dateAppel = $dateAppel;

        return $this;
    }

    public function getHeureAppel(): ?\DateTimeInterface
    {
        return $this->heureAppel;
    }

    public function setHeureAppel(\DateTimeInterface $heureAppel): self
    {
        $this->heureAppel = $heureAppel;

        return $this;
    }

    public function getDureeOrVolume(): ?string
    {
        return $this->dureeOrVolume;
    }

    public function setDureeOrVolume(string $dureeOrVolume): self
    {
        $this->dureeOrVolume = $dureeOrVolume;

        return $this;
    }

    public function getFacture(): ?Facture
    {
        return $this->facture;
    }

    public function setFacture(?Facture $facture): self
    {
        $this->facture = $facture;

        return $this;
    }

    public function getTypeAppel(): ?TypeAppel
    {
        return $this->typeAppel;
    }

    public function setTypeAppel(?TypeAppel $typeAppel): self
    {
        $this->typeAppel = $typeAppel;

        return $this;
    }
}
