<?php

namespace App\Entity;

use App\Repository\FactureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FactureRepository::class)
 */
class Facture
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $compteFacture;

    /**
     * @ORM\Column(type="string", length=15, unique=true)
     */
    private $numeroFacture;

    /**
     * @var Abonne
     * @ORM\ManyToOne(targetEntity="App\Entity\Abonne", inversedBy="listeFacture")
     */
    private $abonne;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Appel", mappedBy="facture")
     */
    private $listeAppels;

    public function __construct()
    {
        $this->listeAppels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompteFacture(): ?string
    {
        return $this->compteFacture;
    }

    public function setCompteFacture(string $compteFacture): self
    {
        $this->compteFacture = $compteFacture;

        return $this;
    }

    public function getNumeroFacture(): ?string
    {
        return $this->numeroFacture;
    }

    public function setNumeroFacture(string $numeroFacture): self
    {
        $this->numeroFacture = $numeroFacture;

        return $this;
    }

    public function getAbonne(): ?Abonne
    {
        return $this->abonne;
    }

    public function setAbonne(?Abonne $abonne): self
    {
        $this->abonne = $abonne;

        return $this;
    }

    /**
     * @return Collection|Appel[]
     */
    public function getListeAppels(): Collection
    {
        return $this->listeAppels;
    }

    public function addListeAppel(Appel $listeAppel): self
    {
        if (!$this->listeAppels->contains($listeAppel)) {
            $this->listeAppels[] = $listeAppel;
            $listeAppel->setFacture($this);
        }

        return $this;
    }

    public function removeListeAppel(Appel $listeAppel): self
    {
        if ($this->listeAppels->contains($listeAppel)) {
            $this->listeAppels->removeElement($listeAppel);
            // set the owning side to null (unless already changed)
            if ($listeAppel->getFacture() === $this) {
                $listeAppel->setFacture(null);
            }
        }

        return $this;
    }
}
