<?php

namespace App\Entity;

use App\Repository\AbonneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AbonneRepository::class)
 */
class Abonne
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10, unique=true)
     */
    private $numeroAbonne;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Facture", mappedBy="abonne")
     */
    private $listeFacture;

    public function __construct()
    {
        $this->listeFacture = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumeroAbonne(): ?string
    {
        return $this->numeroAbonne;
    }

    public function setNumeroAbonne(string $numeroAbonne): self
    {
        $this->numeroAbonne = $numeroAbonne;

        return $this;
    }

    /**
     * @return Collection|Facture[]
     */
    public function getListeFacture(): Collection
    {
        return $this->listeFacture;
    }

    public function addListeFacture(Facture $listeFacture): self
    {
        if (!$this->listeFacture->contains($listeFacture)) {
            $this->listeFacture[] = $listeFacture;
            $listeFacture->setAbonne($this);
        }

        return $this;
    }

    public function removeListeFacture(Facture $listeFacture): self
    {
        if ($this->listeFacture->contains($listeFacture)) {
            $this->listeFacture->removeElement($listeFacture);
            // set the owning side to null (unless already changed)
            if ($listeFacture->getAbonne() === $this) {
                $listeFacture->setAbonne(null);
            }
        }

        return $this;
    }
}
