<?php

namespace App\Entity;

use App\Repository\TypeAppelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeAppelRepository::class)
 */
class TypeAppel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200, unique=true)
     */
    private $type;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Appel", mappedBy="typeAppel")
     */
    private $listeAppels;

    public function __construct()
    {
        $this->listeAppels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Appel[]
     */
    public function getListeAppels(): Collection
    {
        return $this->listeAppels;
    }

    public function addListeAppel(Appel $listeAppel): self
    {
        if (!$this->listeAppels->contains($listeAppel)) {
            $this->listeAppels[] = $listeAppel;
            $listeAppel->setTypeAppel($this);
        }

        return $this;
    }

    public function removeListeAppel(Appel $listeAppel): self
    {
        if ($this->listeAppels->contains($listeAppel)) {
            $this->listeAppels->removeElement($listeAppel);
            // set the owning side to null (unless already changed)
            if ($listeAppel->getTypeAppel() === $this) {
                $listeAppel->setTypeAppel(null);
            }
        }

        return $this;
    }
}
