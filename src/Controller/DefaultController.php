<?php

namespace App\Controller;

use App\Entity\Abonne;
use App\Entity\Appel;
use App\Entity\Facture;
use App\Entity\TypeAppel;
use App\Repository\AbonneRepository;
use App\Repository\FactureRepository;
use App\Repository\TypeAppelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;

class DefaultController extends AbstractController
{
    /**
     * Action appelée pour la page d'accueuil
     * @Route("/", name="homepage")
     */
    public function index()
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * Action gérée par une sous requête qui s'occupe de l'importation du fichier CSV
     * @param Request $request
     * @Route("/import/csv-file", name="import_csv")
     */
    public function importData(Request $request , FactureRepository $factureRepository, AbonneRepository $abonneRepository, TypeAppelRepository $typeAppelRepository)
    {
        //composition du chemin vers le fichier csv a importer en bdd
        $csvPath = $this->getParameter('kernel.project_dir').'/'.$this->getParameter('csv_data');

        //lecture du fichier
        $fichier = \file($csvPath);
        $loopIndex = 0;
        //on parcourt le fichier ligne par ligne
        foreach ($fichier as $row)
        {
            //les lignes à importer commencent à partir de la ligne 4
            if($loopIndex >= 4)
            {
                //on transforme la chaîne en tableau
                $rowData = explode(';',$row);

                //on vérifie d'abord si la facture existe
                $facture = $factureRepository->findOneBy(['numeroFacture' => $rowData[1]]);

                if($facture == null)
                {
                    //Si la facture n'existe pas, on vérifie que l'utilisateur au quel elle est ratachée existe
                    $abonne = $abonneRepository->findOneBy(['numeroAbonne' => $rowData[2]]);
                    if($abonne == null)
                    {
                        //Si l'abonné n'existe pas non plus, dans ce cas on crée de nouveaux enregistrements
                        $abonne = new Abonne();
                        $abonne->setNumeroAbonne($rowData[2]);
                        $this->getDoctrine()->getManager()->persist($abonne);
//                        $this->getDoctrine()->getManager()->flush();
                    }
                    //au cas échéant l'abonné existe donc on insère juste la facture
                    $facture = new Facture();
                    $facture->setAbonne($abonne);
                    $facture->setCompteFacture($rowData[0]);
                    $facture->setNumeroFacture($rowData[1]);
                    $this->getDoctrine()->getManager()->persist($facture);
//                    $this->getDoctrine()->getManager()->flush();
                }
                //Au cas ou la facture existe donc l'abonné existe aussi, on insère juste les informations de l'appel
                $appel = new Appel();
                $appel->setDateAppel(\DateTime::createFromFormat('d/m/Y', $rowData[3]));
                $appel->setHeureAppel(\DateTime::createFromFormat('H:i:s', $rowData[4]));

                //les colonnes $rowData[5] et 6 sont identiques et font référence à la même valeur
                $appel->setDureeOrVolume($rowData[5]);
                $appel->setFacture($facture);

                //on vérifie si le type d'appel existe en base au cas échéant on l'insère et on l'affecte à l'appel
                $typeAppel = $typeAppelRepository->findOneBy(['type' => utf8_encode($rowData[7])]);

                $process = true;
                if($typeAppel == null )
                {
                    //si le type d'appel n'existe pas on le crée
                    $typeAppel = new TypeAppel();
                    $typeAppel->setType(utf8_encode($rowData[7]));
                    $this->getDoctrine()->getManager()->persist($typeAppel);
//                    $this->getDoctrine()->getManager()->flush();
                }
                //on affecte le type d'appel
                $appel->setTypeAppel($typeAppel);
                $this->getDoctrine()->getManager()->persist($appel);
            }
            $loopIndex++;
        }
        $this->getDoctrine()->getManager()->flush();

        $nbEnregistrement = $loopIndex - 4;
        $request->getSession()->getFlashBag()->add('notice_success', 'Le fichier a bien été importé. '.$nbEnregistrement.' nouvelles lignes ont été enregistrées');
        return $this->redirectToRoute('homepage');
    }
}
