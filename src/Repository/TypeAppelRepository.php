<?php

namespace App\Repository;

use App\Entity\TypeAppel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeAppel|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeAppel|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeAppel[]    findAll()
 * @method TypeAppel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeAppelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeAppel::class);
    }

    // /**
    //  * @return TypeAppel[] Returns an array of TypeAppel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeAppel
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
